Story 6:

AC1:

1. Given an empty big size locker, when the customer give the big bag to the assistant, then the assistant should save the bag into the cabinet for the customer and get the ticket and return the ticket to customer
2. Given an empty medium/small size locker, when the customer give the medium/small bag to the assistant, then the assistant should save the bag into the cabinet for the customer and get the ticket and return the ticket to customer
3. Given an empty big/medium/small size locker, when the assistant did not choose right locker size to save the corresponding size bag, should return error message "Can not save xx bag to xx locker "


AC2:

1. Given a valid ticket, when the customer give it to the assistant, the assistant can use the ticket to get the bag from cabinet
2. Given a invalid ticket, when the customer give it to the assistant, then should get the error message

AC3:

1. Given a cabinet and no empty big/medium/small lockers left, when assistant choose to save the bag into the corresponding locker, should get an error message


Story 7:

AC1:

1. Given two empty cabinets, When ask the assistant to save the bag to the second cabinet first, should get the ticket from the second one
2. Given two empty cabinets, when ask the assistant to save two bags and the second cabinet first, should get the two tickets both from the second one
3. Given two cabinets both with one locker left, when ask the assistant to save two bags and the second cabinet first, should get the first ticket from the second one and the second ticket from the first one
4. Given two cabinets with second cabinet full and the first empty, when ask the assistant to save two bags and the second cabinet first, should get the two tickets both from the first one
5. Given two cabinets both full, when ask the assistant to save two bags and the second cabinet first, should get error message