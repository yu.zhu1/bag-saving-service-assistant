package com.twuc.bagSaving;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.ArrayList;

import static com.twuc.bagSaving.CabinetFactory.createCabinetWithPlentyOfCapacity;
import static org.junit.jupiter.api.Assertions.*;

public class StupidAssistantSaveBagTest extends BagSavingArgument{
    @Test
    void should_get_the_ticket_when_the_customer_ask_the_assistant_to_save_big_bag_into_big_locker() {
        Cabinet cabinet = createCabinetWithPlentyOfCapacity();
        Bag bag = new Bag(BagSize.BIG);
        StupidAssistant stupidAssistant = new StupidAssistant(cabinet);
        Ticket ticket = stupidAssistant.saveBag(bag);
        assertNotNull(ticket);
    }

    @ParameterizedTest
    @MethodSource("com.twuc.bagSaving.BagSavingArgument#createAssistantBagSizeAndLockerSize")
    void should_get_the_ticket_when_save_the_medium_and_small_bags_to_corresponding_locker_under_assistance(BagSize bagSize, LockerSize lockerSize) {
        Cabinet cabinet = createCabinetWithPlentyOfCapacity();
        StupidAssistant stupidAssistant = new StupidAssistant(cabinet);
        Ticket ticket = stupidAssistant.saveBag(new Bag(bagSize));
        assertNotNull(ticket);
    }

//    @ParameterizedTest
//    @MethodSource("com.twuc.bagSaving.BagSavingArgument#createAssistantWrongBagSizeAndLockerSize")
//    void should_get_error_message_when_assistant_save_the_bag_to_the_not_corresponded_locker(BagSize bagSize, LockerSize lockerSize) {
//        Cabinet cabinet = createCabinetWithPlentyOfCapacity();
//        StupidAssistant stupidAssistant = new StupidAssistant(cabinet);
//        IllegalArgumentException exception =
//                assertThrows(IllegalArgumentException.class,
//                        () -> stupidAssistant.saveBag2(new Bag(bagSize)));
//        assertEquals(
//                String.format("Cannot save %s bag to %s locker.", bagSize, lockerSize),
//                exception.getMessage());
//    }


    @ParameterizedTest
    @MethodSource("com.twuc.bagSaving.BagSavingArgument#createAssistantBagSizeAndLockerSize")
    void should_get_bag_when_the_assistant_use_the_valid_ticket_to_get_the_bag_from_locker(BagSize bagSize, LockerSize lockerSize) {
        Cabinet cabinet = createCabinetWithPlentyOfCapacity();
        StupidAssistant stupidAssistant = new StupidAssistant(cabinet);
        Bag savedBag = new Bag(bagSize);
        Ticket ticket = stupidAssistant.saveBag(savedBag);
        Bag fetchedBag = stupidAssistant.getBag(ticket);
        assertSame(savedBag, fetchedBag);
    }

    @ParameterizedTest
    @MethodSource("com.twuc.bagSaving.BagSavingArgument#createAssistantBagSizeAndLockerSize")
    void should_get_error_message_when_provide_invalid_ticket_to_assistant(BagSize bagSize, LockerSize lockerSize) {
        Cabinet cabinet = createCabinetWithPlentyOfCapacity();
        StupidAssistant stupidAssistant = new StupidAssistant(cabinet);
        Bag savedBag = new Bag(bagSize);
        stupidAssistant.saveBag(savedBag);
        Ticket invalidTicket = new Ticket();

        final IllegalArgumentException exception = assertThrows(
                IllegalArgumentException.class,
                () -> stupidAssistant.getBag(invalidTicket));
        assertEquals("Invalid ticket.", exception.getMessage());
    }

    @ParameterizedTest
    @MethodSource("com.twuc.bagSaving.BagSavingArgument#createCabinetWithOnlyOneLockerSizeFullForAssistant")
    void should_throw_if_correspond_lockers_are_full(
            Cabinet fullCabinet, BagSize bagSize, LockerSize lockerSize) {
        Bag savedBag = new Bag(bagSize);
        StupidAssistant stupidAssistant = new StupidAssistant(fullCabinet);
        InsufficientLockersException error = assertThrows(
                InsufficientLockersException.class,
                () ->  stupidAssistant.saveBag(savedBag));

        assertEquals("Insufficient empty lockers.", error.getMessage());
    }


    @ParameterizedTest
    @MethodSource("com.twuc.bagSaving.BagSavingArgument#createAssistantBagSizeAndLockerSize")
    void should_get_two_tickets_both_from_the_second_cabinet_if_ask_to_save_the_second_one_first_when_lockers_are_empty(BagSize bagSize, LockerSize lockerSize) {
        Cabinet firstCabinet = createCabinetWithPlentyOfCapacity();
        Cabinet secondCabinet = createCabinetWithPlentyOfCapacity();
        ArrayList<Cabinet> cabinets = new ArrayList<>();
        cabinets.add(firstCabinet);
        cabinets.add(secondCabinet);

        StupidAssistant stupidAssistant = new StupidAssistant(cabinets, new int[] {2,1});
        Bag savedFirstBag = new Bag(bagSize);
        Bag savedSecondBag = new Bag(bagSize);

        Ticket firstTicket = stupidAssistant.saveBag(savedFirstBag);
        Ticket secondTicket = stupidAssistant.saveBag(savedSecondBag);

        assertNotNull(firstTicket);
        assertNotNull(secondTicket);

        Bag firstFetchedBag = secondCabinet.getBag(firstTicket);
        Bag secondFetchedBag = secondCabinet.getBag(secondTicket);

        assertSame(savedFirstBag,firstFetchedBag);
        assertSame(savedSecondBag, secondFetchedBag);

    }
}

