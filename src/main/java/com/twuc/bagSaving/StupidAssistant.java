package com.twuc.bagSaving;

import java.util.ArrayList;
import java.util.List;

public class StupidAssistant {
    private Cabinet cabinet;
    private List<Cabinet> cabinets = new ArrayList<>();
    private int[] order;


    public StupidAssistant() {
    }

    public StupidAssistant(Cabinet cabinet) {
        this.cabinet = cabinet;
    }

    public StupidAssistant(List<Cabinet> cabinets, int[] order) {
        this.cabinets = cabinets;
        this.order = order;
    }


    public Ticket saveBag(Bag bag) {
        BagSize bagSize = bag.getBagSize();
        int sizeNumber = bagSize.getSizeNumber();
        if (isBigBagToBigLocker(sizeNumber)) { return cabinet.save(bag, LockerSize.BIG);}
        if (isMediumBagToMediumLocker(sizeNumber)) { return cabinet.save(bag, LockerSize.MEDIUM);}

        return  cabinet.save(bag, LockerSize.SMALL);
    }

    private boolean isMediumBagToMediumLocker(int sizeNumber) {
        return LockerSize.MEDIUM.getSizeNumber() == sizeNumber;
    }

    private boolean isBigBagToBigLocker(int sizeNumber) {
        return LockerSize.BIG.getSizeNumber() == sizeNumber;
    }

    public Ticket saveBag2(Bag bag) {
        BagSize bagSize = bag.getBagSize();
        int sizeNumber = bagSize.getSizeNumber();
        for (int orderOfSave = 0; orderOfSave < order.length; orderOfSave++ ) {
            if (isBigBagToBigLocker(sizeNumber)) {
//                while (cabinets.get(orderOfSave).getLockers().get(LockerSize.BIG).
                return cabinets.get(orderOfSave).save(bag, LockerSize.BIG);}
        }
        return cabinet.save(bag, LockerSize.SMALL);
    }


    private void validateBagAndLockerSize(BagSize bagSize, LockerSize lockerSize) {
        if (bagSize.getSizeNumber() != lockerSize.getSizeNumber()) {
            throw new IllegalArgumentException(
                    String.format("Cannot save %s bag to %s locker.", bagSize, lockerSize));
        }
    }

    public Bag getBag(Ticket ticket) {
        return cabinet.getBag(ticket);
    }

}


